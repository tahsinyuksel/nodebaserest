/**
* Routes
* All Route Paths
*/

module.exports = (app) => {

	var express = require('express');
	var router = express.Router();

	// Controller Routes Definations
	var homeController = require('./home.js');
	var userController = require('./user.js');

	var apiUrl = app.get('apiUrl');

	// Home Page Route Controller
	router.get('/', homeController.index);

	// User Page Route Controller
	router.get(apiUrl + 'user/:userId', userController.getUser);

	// Default Api Url Route
	router.get(apiUrl, (req, res) => {
		res.status(200).json({ mesaj: "Index Api Page" });
	});

	return router;
}