/**
* Configs
*/

module.exports = (app) => {

	const env = app.get('env') || 'development';

	// Set Default Configs
	app.set('env',process.env.NODE_ENV||'development')
	app.set('apiUrl', '/api/');

	var devConfig = require('./config-dev.js');
    var config = {};

    if (env === 'development') {
        config = devConfig;
    }

    // set custom config
    if(config) {
		app.set('apiUrl', config.apiUrl);
    }

    console.log("Config Env => ", env)

    return config;
}