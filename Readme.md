# Node.js Base REST API

Node.js simple base rest structure. Minimum pure requirements build.

## Feature
- Mvc
- Config environment
- Cross Headers
- Middleware System. All Request filter Middleware. Exapmle: auth, token check

## Todos

 - Create Models
 - Build Service and Dependency Manager Architecture
 - Common Api Response Object
 - Optional; add API Rate Limit, info etc. to headers
 - Write Tests
 - Simple Example Project

## Dependency
- Express
- Morgan Log
- Body-parser Body process

## Version
- Node.js v4.4.5
- Npm 2.15.5

## Installation
- Requires Node.js v4+ to run.
- Download and npm modules install
- Set node_env mode = development|production|test. Default=development. Related source file: config.js
- Run server. Server file: server.js

```sh
$ npm install

$ node server.js
```

## Examples
- Test example controller: UserController, HomeController in routes directory.

## License
MIT

## Contact
Questions, suggestions, comments:


Tahsin Yüksel
info@tahsinyuksel.com
