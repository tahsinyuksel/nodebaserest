var express = require('express');
var bodyParser = require('body-parser');
var logger = require('morgan');
// port = config.port; const PORT = 3000;
const app = express(); // app

// Config set and load
const config = require('./config/config.js')(app);

// Log all requests
app.use(logger('dev'));

// Requests process set format apply
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Enable Cross Origin Resource Sharing
app.all('/*', function(req, res, next) {
  // CORS headers
  res.header("Access-Control-Allow-Origin", "*"); // restrict it to the required domain
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  // Set custom headers for CORS
  res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');
  if (req.method == 'OPTIONS') {
    res.status(200).end();
  } else {
    next();
  }
});

// Auth Middleware - This will check if the token is valid
// Only the requests that start with /api/v1/* will be checked for the token.
// Any URL's that do not follow the below pattern should be avoided unless you 
// are sure that authentication is not needed
app.all(app.get('apiUrl') + '/*', [require('./middlewares/validateRequest')]);

// Use Routes
app.use('/', require('./routes/index')(app));

// Run app Listen Port
app.listen(config.port, () => { console.log(`Express server listening on port ${config.port}.`) });
